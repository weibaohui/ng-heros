import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const heroes = [
            { id: 0,  name: 'xx' },
            { id: 11, name: 'Mr. Nice1' },
            { id: 12, name: 'Narco1' },
            { id: 13, name: 'Bombasto1' },
            { id: 14, name: 'Celeritas1' },
            { id: 15, name: 'Magneta1' },
            { id: 16, name: 'RubberMan1' },
            { id: 17, name: 'Dynam1a' },
            { id: 18, name: 'Dr IQ1' },
            { id: 19, name: 'Magma1' },
            { id: 20, name: 'Tornado1' }
        ];
        return {heroes};
    }
}
